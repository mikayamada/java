package br.com.drogaria.domain;

/**
 *
 * @author 
 */
public class Funcionario {
    private String carteiraTrabalho;
    private String dataAdmisao;
    private Pessoa pessoa;

    public String getCarteiraTrabalho() {
        return carteiraTrabalho;
    }

    public void setCarteiraTrabalho(String carteiraTrabalho) {
        this.carteiraTrabalho = carteiraTrabalho;
    }

    public String getDataAdmisao() {
        return dataAdmisao;
    }

    public void setDataAdmisao(String dataAdmisao) {
        this.dataAdmisao = dataAdmisao;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }
    
}