package dev;

public class TestaBanco {
    
    public static void main(String[] args) {
        
        Gerente g = new Gerente();
        Secretario s = new Secretario();
        PessoaFisica pf = new PessoaFisica();
        PessoaJuridica pj = new PessoaJuridica();
        Autenticador au = new Autenticador();
        
        au.autentica(g);
        au.autentica(s);
        au.autentica(pf);
        au.autentica(pj);
        
    }
    
}
