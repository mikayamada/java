package dev;

public class Cliente implements Usuario {
    
    private String nome;
    private String enderešo;
    private int telefone;
    
    public void trocarSenha() {
        System.out.println("Senha trocada !");
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEnderešo() {
        return enderešo;
    }

    public void setEnderešo(String enderešo) {
        this.enderešo = enderešo;
    }

    public int getTelefone() {
        return telefone;
    }

    public void setTelefone(int telefone) {
        this.telefone = telefone;
    }   
    
}
