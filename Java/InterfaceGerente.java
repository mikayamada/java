package dev;

public class Gerente extends Funcionario implements Usuario {
    
    private int ramal;
    private String departamento;
    
    
    public int getRamal() {
        return ramal;
    }

    public void setRamal(int ramal) {
        this.ramal = ramal;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
        
}
