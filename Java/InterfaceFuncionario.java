package dev;

public abstract class Funcionario implements Usuario {
    
    private String nome;
    private int idade;
    private String carga;
    
    public void trocarSenha() {
        System.out.println("Senha trocada !");
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getCarga() {
        return carga;
    }

    public void setCarga(String carga) {
        this.carga = carga;
    }
    
    
    
}
