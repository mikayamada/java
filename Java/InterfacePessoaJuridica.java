package dev;

public class PessoaJuridica extends Cliente implements Usuario {
    
    private String razaoSocial;
    private String cnpj;
    private String ramoAtividade;
    
    //public void trocarSenha() {
    //    System.out.println("Senha trocada !");
    //}

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRamoAtividade() {
        return ramoAtividade;
    }

    public void setRamoAtividade(String ramoAtividade) {
        this.ramoAtividade = ramoAtividade;
    }
    
    
    
}
