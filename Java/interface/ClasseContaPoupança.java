package dev;

public class ContaPoupança implements Conta {
    
    private String titular;
    private int agencia;
    private int numConta;
    private double saldo;
    private int diaAniversario;
    
    public void deposita(double valor) {
        this.saldo += valor;
    }
    
    public void saca(double valor) {
        if (valor <= this.saldo) {
            this.saldo -= valor;
        } else {
            System.out.println("Saldo insuficiente !");
        }
    }
    
    public void imprimeExtrato() {
        System.out.println("Extrato de Conta Poupança");
        System.out.println("Saldo: " + this.saldo);
        System.out.println("Aniversário: " + this.diaAniversario);
    }
    
    public void render() {
        this.saldo *= 1.0025;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public int getAgencia() {
        return agencia;
    }

    public void setAgencia(int agencia) {
        this.agencia = agencia;
    }

    public int getNumConta() {
        return numConta;
    }

    public void setNumConta(int numConta) {
        this.numConta = numConta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public int getDiaAniversario() {
        return diaAniversario;
    }

    public void setDiaAniversario(int diaAniversario) {
        this.diaAniversario = diaAniversario;
    }
    
    
    
    
}
