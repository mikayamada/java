package dev;

public class TestaBanco {
    
    public static void main(String[] args) {
        ContaCorrente cc = new ContaCorrente();
        ContaPoupanša cp = new ContaPoupanša();
        
        cc.setSaldo(0);
        cp.setSaldo(0);
        
        System.out.println("Saldo CC: " + cc.getSaldo());
        System.out.println("Saldo CP: " + cp.getSaldo());
        
        cc.deposita(10000);
        cp.deposita(5000);
        
        System.out.println("Saldo CC: " + cc.getSaldo());
        System.out.println("Saldo CP: " + cp.getSaldo());  
        
        cc.saca(15000);
        cc.saca(500);
        cp.saca(1000);
        
        System.out.println("Saldo CC: " + cc.getSaldo());
        System.out.println("Saldo CP: " + cp.getSaldo());
        
        GeradorExtrato g = new GeradorExtrato();
        g.gerarExtrato(cp);
        g.gerarExtrato(cc);
        
               
    } 
    
}
