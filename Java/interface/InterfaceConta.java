package dev;

interface Conta {
    
    void deposita(double valor);
    void saca(double valor);
    void imprimeExtrato();
    
}
