package dev;

public class ContaCorrente implements Conta {
    
    private String titular;
    private int agencia;
    private int numConta;
    private double saldo;
    private double limite;
    
    public void deposita(double valor) {
        this.saldo += valor;
    }
    
    public void saca(double valor) {
        if (valor > this.saldo + this.limite) {
            System.out.println("Saldo insuficiente !");
        }else {
            if (valor > this.saldo) {
                valor -= this.saldo;
                this.saldo = 0;
                this.limite -= valor;
            }
        }
        this.saldo -= valor;
    }
    
    public void imprimeExtrato() {
        System.out.println("Extrato de Conta Corrente");
        System.out.println("Saldo: " + this.saldo);
        System.out.println("Limite: " + this.limite);
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public int getAgencia() {
        return agencia;
    }

    public void setAgencia(int agencia) {
        this.agencia = agencia;
    }

    public int getNumConta() {
        return numConta;
    }

    public void setNumConta(int numConta) {
        this.numConta = numConta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }
    
}
